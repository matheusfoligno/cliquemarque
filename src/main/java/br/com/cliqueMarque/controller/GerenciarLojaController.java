package br.com.cliqueMarque.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.SelectEvent;
import org.primefaces.model.DualListModel;

import br.com.cliqueMarque.entity.HoraEntity;
import br.com.cliqueMarque.entity.LojaEntity;
import br.com.cliqueMarque.entity.TempoEntity;
import br.com.cliqueMarque.entity.UsuarioEntity;
import br.com.cliqueMarque.repository.LojaRepository;
import br.com.cliqueMarque.repository.TempoRepository;
import br.com.cliqueMarque.uteis.Uteis;

@Named(value = "gerenciarLojaController")
@SessionScoped
public class GerenciarLojaController implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<TempoEntity> listHorasMarcadas;

	@Inject
	private LojaRepository lojaRepository;

	private DualListModel<String> horas;

	private Date data;
	
	private int entrou = 1;

	@Inject
	private TempoRepository tempoRepository;

	public void init() {
		UsuarioEntity user = getUsuarioSession();
		listHorasMarcadas = tempoRepository.getHorasMarcadasByLojaGerenc(user.getLoja());
		
		if (entrou != 2) {
			horas = new DualListModel<String>(new ArrayList<String>(), new ArrayList<String>());
		} else {
			entrou = 1;
		}

	}

	public void cadastrarTempo() {
		if (data != null) {
			UsuarioEntity user = getUsuarioSession();
			List<TempoEntity> tempos = new ArrayList<TempoEntity>();
			for (String hora : horas.getTarget()) {
				TempoEntity tempo = new TempoEntity();

				tempo.setData(data);

				HoraEntity ho = lojaRepository.getHorasByHora(hora);
				tempo.setHora(ho);
				tempo.setAtivo(1);
				tempo.setLoja(user.getLoja());
				
				tempo.setValor(Math.random() * 100);

				tempoRepository.persist(tempo);
				
				data = null;
				horas = new DualListModel<String>(new ArrayList<String>(), new ArrayList<String>());
			}
			
			Uteis.Mensagem("Tempo cadastrado com sucesso !");
		} else {
			Uteis.MensagemAtencao("Selecione uma data e as horas antes de cadastrar o tempo !");
		}
	}

	public void populaPicker(SelectEvent event) {
		if (data != null) {
			UsuarioEntity user = getUsuarioSession();
			List<String> horasSource = new ArrayList<String>();
			List<String> horasTarget = new ArrayList<String>();

			// horasSource.addAll(lojaRepository.getAllHoras());

			for (String hora : lojaRepository.getAllHoras()) {
				if (tempoRepository.findHoraRelacionamento(hora, user.getLoja(), data)) {
					horasSource.add(hora);
				}
			}

			horas = new DualListModel<String>(horasSource, horasTarget);
			
			entrou = 2;
		} else {
			Uteis.MensagemAtencao("Selecione uma data e as horas antes de cadastrar o tempo !");
		}
	}

	public UsuarioEntity getUsuarioSession() {

		FacesContext facesContext = FacesContext.getCurrentInstance();

		return (UsuarioEntity) facesContext.getExternalContext().getSessionMap().get("usuarioAutenticado");
	}

	public void finalizarCancelar(TempoEntity t) {
		t.setUsuario(null);
		t.setProduto(null);
		t.setAtivo(1);

		tempoRepository.merge(t);
	}

	public List<TempoEntity> getListHorasMarcadas() {
		return listHorasMarcadas;
	}

	public void setListHorasMarcadas(List<TempoEntity> listHorasMarcadas) {
		this.listHorasMarcadas = listHorasMarcadas;
	}

	public DualListModel<String> getHoras() {
		return horas;
	}

	public void setHoras(DualListModel<String> horas) {
		this.horas = horas;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
	
	public int getEntrou() {
		return entrou;
	}
	
	public void setEntrou(int entrou) {
		this.entrou = entrou;
	}

}
