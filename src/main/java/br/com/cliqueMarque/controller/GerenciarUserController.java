package br.com.cliqueMarque.controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.cliqueMarque.entity.TempoEntity;
import br.com.cliqueMarque.entity.UsuarioEntity;
import br.com.cliqueMarque.repository.TempoRepository;

@Named(value = "gerenciarUserController")
@SessionScoped
public class GerenciarUserController implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private List<TempoEntity> listHorasMarcadas;
	
	@Inject
	private TempoRepository tempoRepository;
	
	public void init() {
		listHorasMarcadas = tempoRepository.getHorasMarcadasByUser(getUsuarioSession());
	}
	
	public void finalizarCancelar(TempoEntity t) {
		t.setUsuario(null);
		t.setProduto(null);
		t.setAtivo(1);
		
		tempoRepository.merge(t);
	}
	
	public UsuarioEntity getUsuarioSession() {

		FacesContext facesContext = FacesContext.getCurrentInstance();

		return (UsuarioEntity) facesContext.getExternalContext().getSessionMap().get("usuarioAutenticado");
	}
	
	public List<TempoEntity> getListHorasMarcadas() {
		return listHorasMarcadas;
	}
	
	public void setListHorasMarcadas(List<TempoEntity> listHorasMarcadas) {
		this.listHorasMarcadas = listHorasMarcadas;
	}

}
