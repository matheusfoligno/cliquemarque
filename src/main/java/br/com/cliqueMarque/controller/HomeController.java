package br.com.cliqueMarque.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.DualListModel;

import br.com.cliqueMarque.entity.LojaEntity;
import br.com.cliqueMarque.entity.ProdutoEntity;
import br.com.cliqueMarque.entity.TempoEntity;
import br.com.cliqueMarque.entity.UsuarioEntity;
import br.com.cliqueMarque.repository.LojaRepository;
import br.com.cliqueMarque.repository.TempoRepository;
import br.com.cliqueMarque.repository.UsuarioRepository;
import br.com.cliqueMarque.uteis.Uteis;

@Named(value = "homeController")
@SessionScoped
public class HomeController implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private UsuarioRepository usuarioRepository;
	
	@Inject
	private ProdutoEntity produtoSelecionado;
	
	@Inject
	private TempoRepository tempoRepository;
	
	@Inject
	private LojaRepository lojaRepository;
	
	private DualListModel<String> horas;
	
	private List<TempoEntity> lojasDataGrid;

	private Date data;
	
	private List<ProdutoEntity> listProdutos = new ArrayList<ProdutoEntity>();

	public void redirectHome() {
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("/marqueClique/sistema/home.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void redirectPergunta() {
		UsuarioEntity usuarioEntity = getUsuarioSession();
		if (usuarioEntity.getLoja() != null) {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("/marqueClique/sistema/gerenciarLoja.xhtml");
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("/marqueClique/sistema/home.xhtml");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void redirectCadastroLoja() {
		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("/marqueClique/sistema/cadastrarLoja.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void continuarPrimeiro() {
		if (produtoSelecionado != null) {
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("/marqueClique/sistema/home2.xhtml");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void voltarPrimeiro() {
		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("/marqueClique/sistema/home.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void voltarSegundo() {
		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("/marqueClique/sistema/home2.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void marcarHora(TempoEntity t) {
		UsuarioEntity user = getUsuarioSession();
		
		t.setUsuario(user);
		t.setProduto(produtoSelecionado);
		t.setAtivo(0);
		
		tempoRepository.merge(t);
		
		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("/marqueClique/sistema/gerenciarUser.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void continuar2() {
		if (data != null) {
			lojasDataGrid = new ArrayList<>();
			
			lojasDataGrid = lojaRepository.pesquisaLojas(data, horas, produtoSelecionado);
			
			try {
				FacesContext.getCurrentInstance().getExternalContext()
						.redirect("/marqueClique/sistema/resultadoHome.xhtml");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public void init() {
		UsuarioEntity user = getUsuarioSession();
		user.setPrimeiro_login(2);

		this.usuarioRepository.mergeUsuario(user);
	}
	
	public void init2() {
	    List<String> horasSource = new ArrayList<String>();
        List<String> horasTarget = new ArrayList<String>();
		
		horasSource.addAll(lojaRepository.getAllHoras());
		
		horas = new DualListModel<String>(horasSource, horasTarget);
	}

	public UsuarioEntity getUsuarioSession() {

		FacesContext facesContext = FacesContext.getCurrentInstance();

		return (UsuarioEntity) facesContext.getExternalContext().getSessionMap().get("usuarioAutenticado");
	}

	public List<ProdutoEntity> getListProdutos() {
		return listProdutos;
	}

	public void setListProdutos(List<ProdutoEntity> listProdutos) {
		this.listProdutos = listProdutos;
	}
	
	public ProdutoEntity getProdutoSelecionado() {
		return produtoSelecionado;
	}
	
	public void setProdutoSelecionado(ProdutoEntity produtoSelecionado) {
		this.produtoSelecionado = produtoSelecionado;
	}

	public DualListModel<String> getHoras() {
		return horas;
	}
	
	public void setHoras(DualListModel<String> horas) {
		this.horas = horas;
	}
	
	public Date getData() {
		return data;
	}
	
	public void setData(Date data) {
		this.data = data;
	}
	
	public List<TempoEntity> getLojasDataGrid() {
		return lojasDataGrid;
	}
	
	public void setLojasDataGrid(List<TempoEntity> lojasDataGrid) {
		this.lojasDataGrid = lojasDataGrid;
	}
}
