package br.com.cliqueMarque.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

public class LocalDateTimeConverter2 implements Converter{
	
	//TODO: ver se coloca as horas minutos e segundos tbm
    private final DateTimeFormatter parser = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    @Override
    public Object getAsObject(final FacesContext context, final UIComponent component, final String value) {
   	 if(value.equals(""))
   		 return null;
   	 return LocalDateTime.parse(value, parser);
    }

    @Override
    public String getAsString(final FacesContext context, final UIComponent component, final Object value) {
   	 LocalDateTime date = (LocalDateTime) value;

   	 return parser.format(date);
    }
}

