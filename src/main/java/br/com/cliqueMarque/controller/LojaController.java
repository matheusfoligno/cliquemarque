package br.com.cliqueMarque.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.model.DualListModel;

import br.com.cliqueMarque.entity.HoraEntity;
import br.com.cliqueMarque.entity.LojaEntity;
import br.com.cliqueMarque.entity.ProdutoEntity;
import br.com.cliqueMarque.entity.TempoEntity;
import br.com.cliqueMarque.entity.UsuarioEntity;
import br.com.cliqueMarque.repository.LojaRepository;
import br.com.cliqueMarque.repository.ProdutoRepository;
import br.com.cliqueMarque.repository.UsuarioRepository;
import br.com.cliqueMarque.uteis.Uteis;

@Named(value = "lojaController")
@SessionScoped
public class LojaController implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Inject
	private LojaEntity lojaEntity;
	
	@Inject
	private ProdutoRepository ProdutoRepository;
	
	@Inject
	private UsuarioRepository usuarioRepository;

	@Inject
	private LojaRepository lojaRepository;
	
	private DualListModel<String> horas;
	
	private Date data;
	
	private List<TempoEntity> tempos;
	
	public void cadastrarTempo() {
		if (data != null) {	
			for (String hora : horas.getTarget()) {
				TempoEntity tempo = new TempoEntity();
				
				tempo.setData(data);
				
				HoraEntity ho = lojaRepository.getHorasByHora(hora);
				tempo.setHora(ho);
				tempo.setAtivo(1);
				tempo.setLoja(lojaEntity);
				
				tempo.setValor(Math.random() * 100);
				
				if (tempos == null) {
					tempos = new ArrayList<TempoEntity>();
				}
				tempos.add(tempo);
			}
			
			Uteis.Mensagem("Tempo adicionado com sucesso !");
			
			init();
		} else {
			Uteis.MensagemAtencao("Selecione uma data antes de selecionar os tempos !");
		}
	}
	
	public void cancelarRedirect() {
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("home.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void cadastrarLoja() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		UsuarioEntity user = (UsuarioEntity) facesContext.getExternalContext().getSessionMap()
				.get("usuarioAutenticado");

		if (!verificaCamposLoja(lojaEntity)) {
			Uteis.MensagemAtencao("Todos os campos s�o obrigat�rios !");
		} else {
			
			lojaEntity.setTelefone(lojaEntity.getTelefone().replaceAll("[^0123456789]", ""));
			
			lojaEntity.setTempos(tempos);
			
			lojaEntity.setProdutos(ProdutoRepository.getAll());
			
			for(ProdutoEntity p : lojaEntity.getProdutos()) {
				p.setLoja(lojaEntity);
			}
			
			lojaEntity.setUsuario(user);

			this.lojaRepository.salvar(lojaEntity);
			LojaEntity loja = this.lojaRepository.findLojaByNmFantasia(lojaEntity.getNm_fantasia());
			user.setLoja(loja);
			this.usuarioRepository.mergeUsuario(user);

			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("gerenciarLoja.xhtml");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	public void voltar() {
		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("pergunta.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Boolean verificaCamposLoja(LojaEntity l) {
		if (l.getNm_fantasia() == null || l.getNm_fantasia().equals("") || l.getCnpj() == null || l.getCnpj().equals("")
				|| l.getEndereco() == null || l.getEndereco().equals("") || l.getCidade() == null
				|| l.getCidade().equals("") || l.getEstado() == null || l.getEstado().equals("")) {
			return false;
		} else {
			return true;
		}
	}
	
	public void init() {
        List<String> horasSource = new ArrayList<String>();
        List<String> horasTarget = new ArrayList<String>();
		
		horasSource.addAll(lojaRepository.getAllHoras());
		
		horas = new DualListModel<String>(horasSource, horasTarget);
	}

	public LojaEntity getLojaEntity() {
		return lojaEntity;
	}

	public void setLojaEntity(LojaEntity lojaEntity) {
		this.lojaEntity = lojaEntity;
	}
	
	public DualListModel<String> getHoras() {
		return horas;
	}
	
	public void setHoras(DualListModel<String> horas) {
		this.horas = horas;
	}
	
	public Date getData() {
		return data;
	}
	
	public void setData(Date data) {
		this.data = data;
	}
	
	public List<TempoEntity> getTempos() {
		return tempos;
	}
	
	public void setTempos(List<TempoEntity> tempos) {
		this.tempos = tempos;
	}

}
