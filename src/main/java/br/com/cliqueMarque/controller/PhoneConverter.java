package br.com.cliqueMarque.controller;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

public class PhoneConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		
		 throw new UnsupportedOperationException("Ainda n�o implementado...");
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		
		String fone = (String) value; 
		
		if(!fone.isEmpty() && fone != null){ // faz tratamento dos dados
			String ddd = fone.substring(0, 2); // os dois primeiros digitos 87
			String numeroParte1 = ""; 
			String numeroParte2 = "";
			
			if(fone.length() == 11){ // se o numero estiver no novo padrao ( nono digito )
				numeroParte1 = fone.substring(2, 7); // os 5 primeiro digito 
				numeroParte2 = fone.substring(7, 11); // os 4 utimos digitos
			} else {
				numeroParte1 = fone.substring(2, 6); // os 4 primeiro digito 
				numeroParte2 = fone.substring(6, 10); // os 4 utimos digitos
			}
			return "(" + ddd + ") " + numeroParte1 + " - " + numeroParte2;
		}
		
		return "";
	}

}
