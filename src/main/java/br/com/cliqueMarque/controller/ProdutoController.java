package br.com.cliqueMarque.controller;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import br.com.cliqueMarque.entity.ProdutoEntity;
import br.com.cliqueMarque.repository.ProdutoRepository;

@Named(value = "produtoController")
@SessionScoped
public class ProdutoController implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Inject
	private ProdutoRepository produtoRepository;
	
	private List<ProdutoEntity> produtos;
	
	public void init() {
		setProdutos(produtoRepository.getAll());
	}

	public ProdutoRepository getProdutoRepository() {
		return produtoRepository;
	}

	public void setProdutoRepository(ProdutoRepository produtoRepository) {
		this.produtoRepository = produtoRepository;
	}

	public List<ProdutoEntity> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<ProdutoEntity> produtos) {
		this.produtos = produtos;
	}

}
