package br.com.cliqueMarque.controller;

import java.io.IOException;
import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.lang3.StringUtils;

import br.com.cliqueMarque.entity.LojaEntity;
import br.com.cliqueMarque.entity.UsuarioEntity;
import br.com.cliqueMarque.repository.LojaRepository;
import br.com.cliqueMarque.repository.UsuarioRepository;
import br.com.cliqueMarque.uteis.Uteis;

@Named(value = "usuarioController")
@SessionScoped
public class UsuarioController implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private UsuarioRepository usuarioRepository;

	@Inject
	private UsuarioEntity usuarioEntity;

	public UsuarioEntity GetUsuarioSession() {

		FacesContext facesContext = FacesContext.getCurrentInstance();

		return (UsuarioEntity) facesContext.getExternalContext().getSessionMap().get("usuarioAutenticado");
	}

	public String Logout() {

		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();

		return "/index.xhtml?faces-redirect=true";
	}

	public void voltar() {

		try {
			FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String EfetuarLogin() {

		if (StringUtils.isEmpty(usuarioEntity.getEmail()) || StringUtils.isBlank(usuarioEntity.getEmail())) {

			Uteis.Mensagem("Favor informar o email !");
			return null;
		} else if (StringUtils.isEmpty(usuarioEntity.getSenha()) || StringUtils.isBlank(usuarioEntity.getSenha())) {

			Uteis.Mensagem("Favor informar a senha !");
			return null;
		} else {

			usuarioEntity = usuarioRepository.ValidaUsuario(usuarioEntity);

			if (usuarioEntity != null) {

				FacesContext facesContext = FacesContext.getCurrentInstance();

				facesContext.getExternalContext().getSessionMap().put("usuarioAutenticado", usuarioEntity);

				if (usuarioEntity.getPrimeiro_login() == 1) {
					return "sistema/pergunta?faces-redirect=true";
				} else {
					if (usuarioEntity.getLoja() != null) {
						return "sistema/gerenciarLoja?faces-redirect=true";
					} else {
						return "sistema/home?faces-redirect=true";
					}
				}
			} else {

				Uteis.Mensagem("N�o foi poss�vel efetuar o login com esse usu�rio e senha!");
				return null;
			}
		}

	}

	public String cadastrar() {
		return "cadastrar?faces-redirect=true";
	}

	public void cadastrarUsuario() {

		if (verificaCampos()) {
			
			usuarioEntity.setTelefone(usuarioEntity.getTelefone().replaceAll("[^0123456789]", ""));
			
			// 1=Sim, 0=N�o
			this.usuarioEntity.setPrimeiro_login(1);

			usuarioRepository.salvarUsuario(this.usuarioEntity);

			Uteis.MensagemInfo("Registro cadastrado com sucesso");

			try {
				FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			Uteis.MensagemAtencao("Preencha todos os campos obrigat�rios !");
		}
	}

	public Boolean verificaCampos() {
		if (usuarioEntity.getNome() == null || usuarioEntity.getNome().equals("") || usuarioEntity.getEmail() == null
				|| usuarioEntity.getEmail().equals("") || usuarioEntity.getSenha() == null
				|| usuarioEntity.getSenha().equals("") || usuarioEntity.getEndereco() == null
				|| usuarioEntity.getEndereco().equals("") || usuarioEntity.getSexo() == null
				|| usuarioEntity.getSexo().equals("") || usuarioEntity.getTelefone() == null
				|| usuarioEntity.getTelefone().equals("")) {
			return false;
		} else {
			return true;
		}
	}

	public String temLoja() {
		UsuarioEntity user = GetUsuarioSession();

		if (user.getLoja() != null) {
			return "sim";
		} else {
			return "nao";
		}
	}

	public UsuarioEntity getUsuarioEntity() {
		return usuarioEntity;
	}

	public void setUsuarioEntity(UsuarioEntity usuarioEntity) {
		this.usuarioEntity = usuarioEntity;
	}

	public void init() {
		usuarioEntity = new UsuarioEntity();
	}
}
