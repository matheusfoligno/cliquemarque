package br.com.cliqueMarque.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Table(name="loja")
@Entity
public class LojaEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="id")
	private String id;
	
	@Column(name="nm_fantasia")
	private String nm_fantasia;
	
	@Column(name="cnpj")
	private String cnpj;
	
	@Column(name="endereco")
	private String endereco;
	
	@Column(name="cidade")
	private String cidade;
	
	@Column(name="estado")
	private String estado;
	
	@OneToOne
	@JoinColumn(name = "id_usuario")
	private UsuarioEntity usuario;
	
	@OneToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "loja_produto", joinColumns = { @JoinColumn(name = "loja_id") }, inverseJoinColumns = {
			@JoinColumn(name = "produto_id") })
	private List<ProdutoEntity> produtos;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "tempo_loja", joinColumns = { @JoinColumn(name = "id_tempo") }, inverseJoinColumns = {
			@JoinColumn(name = "id_usuario") })
	private List<TempoEntity> tempos;
	
	@Column(name="telefone")
	private String telefone;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNm_fantasia() {
		return nm_fantasia;
	}

	public void setNm_fantasia(String nm_fantasia) {
		this.nm_fantasia = nm_fantasia;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public UsuarioEntity getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioEntity usuario) {
		this.usuario = usuario;
	}
	
	public List<ProdutoEntity> getProdutos() {
		return produtos;
	}
	
	public void setProdutos(List<ProdutoEntity> produtos) {
		this.produtos = produtos;
	}
	
	public List<TempoEntity> getTempos() {
		return tempos;
	}
	
	public void setTempos(List<TempoEntity> tempos) {
		this.tempos = tempos;
	}
	
	public String getTelefone() {
		return telefone;
	}
	
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
}
