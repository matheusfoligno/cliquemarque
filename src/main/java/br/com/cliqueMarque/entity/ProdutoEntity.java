package br.com.cliqueMarque.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Table(name="produto")
@Entity
public class ProdutoEntity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="id")
	private String id;
	
	@Column(name="nm_produto")
	private String nm_produto;
	
	@Column(name="valor")
	private Double valor;
	
//	@OneToOne
//	@JoinColumn(name = "id")
//	private UsuarioEntity usuario;
	
	@OneToOne
	@JoinColumn(name = "id_loja")
	private LojaEntity loja;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNm_produto() {
		return nm_produto;
	}

	public void setNm_produto(String nm_produto) {
		this.nm_produto = nm_produto;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}
	
//	public UsuarioEntity getUsuario() {
//		return usuario;
//	}
//	
//	public void setUsuario(UsuarioEntity usuario) {
//		this.usuario = usuario;
//	}
	
	public LojaEntity getLoja() {
		return loja;
	}
	
	public void setLoja(LojaEntity loja) {
		this.loja = loja;
	}
}
