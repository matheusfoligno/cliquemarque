package br.com.cliqueMarque.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Table(name="tempo")
@Entity
public class TempoEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue
	@Column(name="id")
	private String id;
	
	@OneToOne
	@JoinColumn(name = "id_hora")
	private HoraEntity hora;
	
	@Column(name="ativo")
	private int ativo;
	
	@OneToOne
	@JoinColumn(name = "id_usuario")
	private UsuarioEntity usuario;
	
	@OneToOne
	@JoinColumn(name = "id_loja")
	private LojaEntity loja;
	
	@Column(name="data")
	private Date data;
	
	@OneToOne
	@JoinColumn(name = "id_produto")
	private ProdutoEntity produto;
	
	@Column(name="valor")
	private Double valor;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public HoraEntity getHora() {
		return hora;
	}

	public void setHora(HoraEntity hora) {
		this.hora = hora;
	}

	public int getAtivo() {
		return ativo;
	}

	public void setAtivo(int ativo) {
		this.ativo = ativo;
	}

	public UsuarioEntity getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioEntity usuario) {
		this.usuario = usuario;
	}
	
	public Date getData() {
		return data;
	}
	
	public void setData(Date data) {
		this.data = data;
	}
	
	public LojaEntity getLoja() {
		return loja;
	}
	
	public void setLoja(LojaEntity loja) {
		this.loja = loja;
	}
	
	public ProdutoEntity getProduto() {
		return produto;
	}
	
	public void setProduto(ProdutoEntity produto) {
		this.produto = produto;
	}
	
	public Double getValor() {
		return valor;
	}
	
	public void setValor(Double valor) {
		this.valor = valor;
	}

}
