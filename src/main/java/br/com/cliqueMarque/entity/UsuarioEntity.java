package br.com.cliqueMarque.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Table(name="usuario")
@Entity
@NamedQuery(name = "UsuarioEntity.findUser", 
query= "SELECT u FROM UsuarioEntity u WHERE u.email = :email AND u.senha = :senha")
public class UsuarioEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name="id")
	private String id;
	
	@Column(name="nome")
	private String nome;
	
	@Column(name="email")
	private String email;
	
	@Column(name="senha")
	private String senha;
	
	@Column(name="sexo")
	private String sexo;
	
	@Column(name="endereco")
	private String endereco;
	
	@Column(name="primeiro_login")
	private int primeiro_login;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_loja")
	private LojaEntity loja;
	
	//1-Visao Loja, 0-Visao User
	@Column(name="visao")
	private int visao;
	
	@Column(name="telefone_cel")
	private String telefone;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public int getPrimeiro_login() {
		return primeiro_login;
	}

	public void setPrimeiro_login(int primeiro_login) {
		this.primeiro_login = primeiro_login;
	}

	public LojaEntity getLoja() {
		return loja;
	}

	public void setLoja(LojaEntity loja) {
		this.loja = loja;
	}
	
	public int getVisao() {
		return visao;
	}
	
	public void setVisao(int visao) {
		this.visao = visao;
	}
	
	public String getTelefone() {
		return telefone;
	}
	
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	
}
