package br.com.cliqueMarque.repository;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.primefaces.model.DualListModel;

import br.com.cliqueMarque.entity.HoraEntity;
import br.com.cliqueMarque.entity.LojaEntity;
import br.com.cliqueMarque.entity.ProdutoEntity;
import br.com.cliqueMarque.entity.TempoEntity;
import br.com.cliqueMarque.uteis.Uteis;

public class LojaRepository implements Serializable {
	
	private static final long serialVersionUID = 1L;

	public void salvar(LojaEntity loja) {
		
		EntityManager em = Uteis.JpaEntityManager();
		
		if (em.getTransaction().isActive()) {
			em.close();
		}
 
		em.getTransaction().begin();
		em.persist(loja);
		em.getTransaction().commit();
		em.close();
		
	}
	
	public LojaEntity findLojaByNmFantasia(String nmFantasia) {
		EntityManager em = Uteis.JpaEntityManager();
		
		if (em.getTransaction().isActive()) {
			em.close();
		}
 
		em.getTransaction().begin();
		LojaEntity loja = (LojaEntity) em.createQuery("SELECT l FROM LojaEntity l WHERE l.nm_fantasia = :nmFantasia")
				.setParameter("nmFantasia", nmFantasia).setMaxResults(1).getSingleResult();
		em.getTransaction().commit();
		em.close();
		
		return loja;
	}
	
	public void merge(LojaEntity loja) {
		
		EntityManager em = Uteis.JpaEntityManager();
		
		if (em.getTransaction().isActive()) {
			em.close();
		}
 
		em.getTransaction().begin();
		em.merge(loja);
		em.getTransaction().commit();
		em.close();
		
	}
	
	public List<String> getAllHoras() {
		
		EntityManager em = Uteis.JpaEntityManager();
		
		if (em.getTransaction().isActive()) {
			em.close();
		}
 
		em.getTransaction().begin();
		List<String> horas = em.createQuery("SELECT h.hora FROM HoraEntity h").getResultList();
		em.getTransaction().commit();
		em.close();
		
		return horas;
	}
	
	public List<TempoEntity> pesquisaLojas(Date data, DualListModel<String> horas, ProdutoEntity produtoSelecionado) {
		EntityManager em = Uteis.JpaEntityManager();
		
		if (em.getTransaction().isActive()) {
			em.close();
		}
 
		em.getTransaction().begin();
		
		StringBuilder sb = new StringBuilder();
//		sb.append("SELECT distinct t FROM LojaEntity l INNER JOIN l.produtos p INNER JOIN l.tempos t ");
//		sb.append(" INNER JOIN t.hora h WHERE p.nm_produto = :produto AND t.ativo = 1 AND t.data = :data ");
		
		sb.append("SELECT distinct t FROM TempoEntity t INNER JOIN t.loja l INNER JOIN l.produtos p");
		sb.append(" INNER JOIN t.hora h WHERE p.nm_produto = :produto AND t.ativo = 1 AND t.data = :data ");
		
		if (!horas.getTarget().isEmpty()) {
			sb.append("AND h.hora in :horas");
		}
		
		Query query = em.createQuery(sb.toString());
		query.setParameter("produto", produtoSelecionado.getNm_produto());
		query.setParameter("data", data, TemporalType.DATE);
		
		if (!horas.getTarget().isEmpty()) {
			query.setParameter("horas", horas.getTarget());
		}
		
		List<TempoEntity> lojas = query.getResultList();
		
		em.getTransaction().commit();
		em.close();
		
		return lojas;
	}
	
	public HoraEntity getHorasByHora(String hora) {
		
		EntityManager em = Uteis.JpaEntityManager();
		
		if (em.getTransaction().isActive()) {
			em.close();
		}
 
		em.getTransaction().begin();
		HoraEntity h = (HoraEntity) em.createQuery("SELECT h FROM HoraEntity h WHERE h.hora = :hora")
				.setParameter("hora", hora).setMaxResults(1).getSingleResult();
		em.getTransaction().commit();
		em.close();
		
		return h;
	}

}
