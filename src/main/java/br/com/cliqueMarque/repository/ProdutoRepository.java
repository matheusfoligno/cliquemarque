package br.com.cliqueMarque.repository;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;

import br.com.cliqueMarque.entity.ProdutoEntity;
import br.com.cliqueMarque.uteis.Uteis;

public class ProdutoRepository implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public List<ProdutoEntity> getAll() {
		
		EntityManager em = Uteis.JpaEntityManager();
		
		if (em.getTransaction().isActive()) {
			em.close();
		}
		
		em.getTransaction().begin();
		List<ProdutoEntity> produtos = em.createQuery("SELECT p FROM ProdutoEntity p").getResultList();
		em.getTransaction().commit();
		em.close();
		
		return produtos;
	}

}
