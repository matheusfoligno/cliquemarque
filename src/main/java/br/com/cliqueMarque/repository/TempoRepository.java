package br.com.cliqueMarque.repository;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TemporalType;

import br.com.cliqueMarque.entity.HoraEntity;
import br.com.cliqueMarque.entity.LojaEntity;
import br.com.cliqueMarque.entity.TempoEntity;
import br.com.cliqueMarque.entity.UsuarioEntity;
import br.com.cliqueMarque.uteis.Uteis;

public class TempoRepository implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public void merge(TempoEntity tempo) {
		
		EntityManager em = Uteis.JpaEntityManager();
		
		if (em.getTransaction().isActive()) {
			em.close();
		}
 
		em.getTransaction().begin();
		em.merge(tempo);
		em.getTransaction().commit();
		em.close();
		
	}
	
	public void persist(TempoEntity tempo) {
		
		EntityManager em = Uteis.JpaEntityManager();
		
		if (em.getTransaction().isActive()) {
			em.close();
		}
 
		em.getTransaction().begin();
		em.persist(tempo);
		em.getTransaction().commit();
		em.close();
		
	}
	
	public List<TempoEntity> getHorasMarcadasByUser(UsuarioEntity user) {
		
		EntityManager em = Uteis.JpaEntityManager();
		
		if (em.getTransaction().isActive()) {
			em.close();
		}
 
		em.getTransaction().begin();
		
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT t FROM TempoEntity t INNER JOIN t.usuario u WHERE u.email = :email");
		
		List<TempoEntity> tempos = em.createQuery(sb.toString()).setParameter("email", user.getEmail()).getResultList();
		
		em.getTransaction().commit();
		em.close();
		
		return tempos;
	}
	
	public List<TempoEntity> getHorasMarcadasByLoja(LojaEntity loja) {
		
		EntityManager em = Uteis.JpaEntityManager();
		
		if (em.getTransaction().isActive()) {
			em.close();
		}
 
		em.getTransaction().begin();
		
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT t FROM TempoEntity t INNER JOIN t.loja l WHERE l.nm_fantasia = :nmFantasia");
		
		List<TempoEntity> tempos = em.createQuery(sb.toString()).setParameter("nmFantasia", loja.getNm_fantasia()).getResultList();
		
		em.getTransaction().commit();
		em.close();
		
		return tempos;
	}
	
	public List<TempoEntity> getHorasMarcadasByLojaGerenc(LojaEntity loja) {
		
		EntityManager em = Uteis.JpaEntityManager();
		
		if (em.getTransaction().isActive()) {
			em.close();
		}
 
		em.getTransaction().begin();
		
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT t FROM TempoEntity t INNER JOIN t.loja l WHERE l.nm_fantasia = :nmFantasia AND"
				+ " t.ativo = 0 AND t.usuario is not null");
		
		List<TempoEntity> tempos = em.createQuery(sb.toString()).setParameter("nmFantasia", loja.getNm_fantasia()).getResultList();
		
		em.getTransaction().commit();
		em.close();
		
		return tempos;
	}

	public Boolean findHoraRelacionamento(String hora, LojaEntity loja, Date data) {
		EntityManager em = Uteis.JpaEntityManager();
		
		if (em.getTransaction().isActive()) {
			em.close();
		}
 
		em.getTransaction().begin();
		
		StringBuilder sb = new StringBuilder();
		sb.append("SELECT h FROM TempoEntity t INNER JOIN t.loja l INNER JOIN t.hora h WHERE l.nm_fantasia = :nmFantasia ");
		sb.append("AND h.hora = :hora AND t.ativo = 1 AND t.usuario is null AND t.data = :data");
		
		List<HoraEntity> tempos = em.createQuery(sb.toString())
				.setParameter("nmFantasia", loja.getNm_fantasia())
				.setParameter("hora", hora)
				.setParameter("data", data, TemporalType.DATE).getResultList();
		
		em.getTransaction().commit();
		em.close();
		
		if (tempos == null || tempos.isEmpty()) {
			return true;
		} else {
			return false;
		}
	}

}
