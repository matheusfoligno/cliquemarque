package br.com.cliqueMarque.repository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.cliqueMarque.entity.UsuarioEntity;
import br.com.cliqueMarque.uteis.Uteis;
 
 
public class UsuarioRepository implements Serializable {
 
	@Inject
	private UsuarioEntity usuarioEntity;
 
	private static final long serialVersionUID = 1L;
 
	public UsuarioEntity ValidaUsuario(UsuarioEntity usuario){
 
		try {
			
			EntityManager entityManager = Uteis.JpaEntityManager();
			
			if (entityManager.getTransaction().isActive()) {
				entityManager.close();
			}
			
			entityManager.getTransaction().begin();

			//RETORNA O USU�RIO SE FOR LOCALIZADO
			UsuarioEntity usr = (UsuarioEntity) entityManager.
					createQuery("SELECT u FROM UsuarioEntity u WHERE u.email = :email AND u.senha = :senha")
					.setParameter("email", usuario.getEmail()).setParameter("senha", usuario.getSenha()).getSingleResult();
			
			entityManager.getTransaction().commit();
			entityManager.close();
			
			return usr;
 
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public void salvarUsuario(UsuarioEntity usuario) {
		
		EntityManager em = Uteis.JpaEntityManager();
		
		if (em.getTransaction().isActive()) {
			em.close();
		}
 
		em.getTransaction().begin();
		em.persist(usuario);
		em.getTransaction().commit();
		em.close();
		
	}
	
	public void mergeUsuario(UsuarioEntity usuario) {
		
		EntityManager em = Uteis.JpaEntityManager();
		
		if (em.getTransaction().isActive()) {
			em.close();
		}
 
		em.getTransaction().begin();
		em.merge(usuario);
		em.getTransaction().commit();
		em.close();
		
	}
	
	public UsuarioEntity getUltimoPersist() {
		
		EntityManager em = Uteis.JpaEntityManager();
		
		if (em.getTransaction().isActive()) {
			em.close();
		}
 
		em.getTransaction().begin();
		UsuarioEntity user = (UsuarioEntity) em.createQuery("Select u From UsuarioEntity u ORDER BY u.id DESC")
				.setMaxResults(1).getSingleResult();
		em.getTransaction().commit();
		em.close();
		
		return user;
	}
	
	public List<UsuarioEntity> getUsuarios() {
		EntityManager em = Uteis.JpaEntityManager();
		
		if (em.getTransaction().isActive()) {
			em.close();
		}
		
		List<UsuarioEntity> usuarios = new ArrayList<UsuarioEntity>();
		
		em.getTransaction().begin();
		Query query = em.createQuery("SELECT u FROM UsuarioEntity u");
		
		usuarios = query.getResultList();
		
		em.getTransaction().commit();
		em.close();
		
		return usuarios;
		
	}
	
	public String getRoleById(String id) {
		EntityManager em = Uteis.JpaEntityManager();
		
		if (em.getTransaction().isActive()) {
			em.close();
		}
		
		em.getTransaction().begin();
		Query query = em.createQuery("SELECT u.role FROM UsuarioEntity u WHERE u.codigo = :id");
		query.setParameter("id", id);
		String role = (String) query.getSingleResult();
		
		em.getTransaction().commit();
		em.close();
		
		return role;
	}
}